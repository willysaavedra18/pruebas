import time
import os

id = []
RuT = []
NonRazon = []
Fecha = []
Tiphuevo = []
cantidad = []
direccion = []
PrecioTo = []
nombre = 0
login = True
while login == True:
    os.system("cls")
    print("==================================")
    print("        SISTEMA DE LOGIN          ")
    print("==================================\n")
    user = input("Ingrese Usuario: ")
    pas = input("Ingrese Contraseña: ")
    if user == "admin" and pas == "1234":
        login = False
        os.system("cls")

        cont = 0
        while cont == 0:
            print(" ===================================")
            print("|| BIENVENIDO A LA EMPRESA ALL EGG ||")
            print(" ===================================\n")
            print("a) Asignación de precios de huevos")
            print("b) Creación de despachos")
            print("c) Listar huevos")
            print("d) Listar despacho")
            print("e) Salir del menu\n")
            opc = str(input("Ingrese la opción deseada: "))

            if opc == "a":
                os.system("cls")
                print("==========================================")
                print("   BIENVENIDO A LA ASIGNACIÓN DE HUEVOS   ")
                print("==========================================\n")
                while True:
                    try:
                        hgallina= int(input("Ingrese el precio del huevo de gallina: "))
                        if hgallina >= 50 :
                            break
                        else:
                            print("El precio mínimo del  huevo es de $50.")
                    except ValueError:
                        print("Ingrese un precio valido")
                while True:
                    try:
                        hpato= int(input("Ingrese el precio del huevo de pato: "))
                        if hpato >= 150 :
                            
                            break
                        else:
                            print("El precio mínimo del  huevo es de $150.")
                    except ValueError:
                        print("Ingrese un precio valido")
                while True:
                    try:
                        hcodorniz= int(input("Ingrese el precio del huevo de codorniz: "))
                        if hcodorniz >= 50:
                            
                            break
                        else:
                            print("El precio mínimo del  huevo es de $50.")
                    except ValueError:
                        print("Ingrese un precio valido")
                while True:
                    try:
                        havestruz= int(input("Ingrese el precio del huevo de avestruz: "))
                        if havestruz >= 800:
                            
                            break
                        else:
                            print("El precio mínimo del  huevo es de $800.")
                    except ValueError:
                        print("Ingrese un precio valido")
                time.sleep(2)
                os.system("cls")

            elif opc == "b":
                id.append(nombre)
                os.system("cls")
                print("===========================")
                print("   CREACIÓN DE DESPACHOS   ")
                print("===========================\n")

                while True:
                    rutt= input("Ingresa tu rut: ")
                    if rutt != "":
                        RuT.append(rutt)
                        break
                    else:
                        print("No puede dejar el rut en blanco")
                while True:
                    nomrazon= input("Ingresa Nombre o Razon Social: ")
                    if nomrazon != "":
                        NonRazon.append(nomrazon)
                        break
                    else:
                        print("No puede dejar Nombre o razon social en blanco")
                while True:
                    fech= input("Ingrese la fecha de compra: ")
                    if fech != "":
                        Fecha.append(fech)
                        break
                    else:
                        print("No puede dejar la fecha en blanco")
                while True:
                    try:
                        canhuevos= int(input("Ingrese la cantidad  huevos: "))
                        if canhuevos >= 50 and canhuevos < 10000:
                            cantidad.append(canhuevos)
                            break
                        else:
                            print("El cantidad mínima es de 50 y maxima 10000")
                    except ValueError:
                        print("Ingrese una cantidad valida")
                while True:
                    dire = input("Ingrese su dirección: ")
                    if dire != "":
                        direccion.append(dire)
                        break
                    else: 
                        print("No puede dejar dirección en blanco")
                while True: 
                    print("===============")
                    print("   -Gallina    ")
                    print("   -Pato       ")
                    print("   -Codorniz   ")
                    print("   -Avestruz   ")
                    print("===============\n")
                    tiphuevo=input("Ingrese el tipo de huevo: ")
                    if tiphuevo == "gallina":
                        Tiphuevo.append(tiphuevo)
                        conve = input("Tiene convenio? si / no:  ")
                        if conve == "si":
                            porce = hgallina * canhuevos / 10
                            precio = (canhuevos * hgallina) - porce
                            PrecioTo.append(precio)
                            break
                        elif conve == "no":
                            precio = (canhuevos * hgallina)
                            PrecioTo.append(precio)
                            break
                        else:
                            print("¡Ingrese si o no! ")

                    elif tiphuevo == "pato":
                        Tiphuevo.append(tiphuevo)
                        conve = input("Tiene convenio? si / no:  ")
                        if conve == "si":
                            porce = hpato * canhuevos / 10
                            precio = (canhuevos * hpato) - porce
                            PrecioTo.append(precio)
                            break
                        elif conve == "no":
                            precio = (canhuevos * hpato)
                            PrecioTo.append(precio)
                            break
                        else:
                            print("¡Ingrese si o no! ")

                    elif tiphuevo == "codorniz":
                        Tiphuevo.append(tiphuevo)
                        conve = input("Tiene convenio? si / no:  ")
                        if conve == "si":
                            porce = hcodorniz * canhuevos / 10
                            precio = (canhuevos * hcodorniz) - porce
                            PrecioTo.append(precio)
                            break
                        elif conve == "no":
                            precio = (canhuevos * hcodorniz)
                            PrecioTo.append(precio)
                            break
                        else:
                            print("¡Ingrese si o no! ")

                    elif tiphuevo== "avestruz":
                        Tiphuevo.append(tiphuevo)
                        conve = input("Tiene convenio? si / no:  ")
                        if conve == "si":
                            porce = havestruz * canhuevos / 10
                            precio = (canhuevos * havestruz) - porce
                            PrecioTo.append(precio)
                            break
                        elif conve == "no":
                            precio = (canhuevos * havestruz)
                            PrecioTo.append(precio)
                            break
                        else:
                            print("¡Ingrese si o no! ")
                    else:
                        print("¡Opción no valida!")

                time.sleep(2)
                os.system("cls")
                print("==========================================================")
                print("   ¡El registro del despacho se realizo corrrectamente!   ")
                print("==========================================================")
                print("Cargando...")
                time.sleep(2)
                os.system("cls")
                
                nombre += 1

            elif opc == "c":
                os.system("cls")
                print("=====================================")
                print("  TIPO DE HUEVO  |  precio unitario  ")
                print("=====================================")
                print("   GALLINA       |", hgallina)
                print("   PATO          |", hpato)
                print("   CODORNIZ      |", hcodorniz)
                print("   AVESTRUZ      |", havestruz )
                os.system("\n pause")
                os.system("cls")
            
            elif opc == "d":
                if len(id) <= 0:
                    os.system("cls")
                    print("==================================")
                    print("   NO HAY DESPACHOS REGISTRADOS   ")
                    print("==================================")
                    time.sleep(2)
                    os.system("cls")
                    continue
                os.system("cls")
                print("+--------------------+-------------------+---------------------------+------------+-------------------+-------------------+-----------------------+------------------+")
                print("|  ID DE DESPACHO    |       RUT         |   NOMBRE O RAZON SOCIAL   |    FECHA   |     DIRECCIÓN     |   TIPO DE HUEVO   |   cantidad de huevo   |   PRECIO TOTAL   |")
                print("+--------------------+-------------------+---------------------------+------------+-------------------+-------------------+-----------------------+------------------+")
                ctd = 0               
                while ctd < len(id):
                    nombre1 = id[ctd]
                    rutt = RuT[ctd]
                    nomrazon = NonRazon[ctd]
                    fech = Fecha[ctd]
                    dire = direccion[ctd]
                    tiphuevo = Tiphuevo[ctd]
                    canhuevos = cantidad[ctd]
                    precio = PrecioTo[ctd]
                    
                    print ("|{:<20}|{:<19}|{:<27}|{:<12}|{:<19}|{:<19}|{:<23}|{:<18}|".format(nombre1, rutt, nomrazon, fech, dire, tiphuevo, canhuevos, precio))
                    print("+--------------------+-------------------+---------------------------+------------+-------------------+-------------------+-----------------------+------------------+")

                    ctd += 1
                os.system("pause")
                os.system("cls")
                
            elif opc == "e":
                os.system("cls")
                print("=========================")
                print("   SALIÓ DEL PROGRAMA    ")
                print("=========================\n")
                cont = 2
            else:
                print("¡Ingrese una opción válida!")
                time.sleep(1.5)
                os.system("cls")
        cont += 1
    else:
        print("\n¡USUARIO O CONTRASEÑA INCORRECTOS!")
        time.sleep(2)

